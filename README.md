# git basics

Learning:

- Installing Git
- git config
- Initializing Git
- checkout a repository
- add and commit
- Status
- pushing changes
- branching
- rebase and merge
- tagging
- log
- replace local changes
- tig - Text-mode interface for git